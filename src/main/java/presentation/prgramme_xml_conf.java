package presentation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import service_XmlConfig.Departement;
import service_XmlConfig.Faculte;
import service_XmlConfig.Universite;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;

public class prgramme_xml_conf {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"spring.xml"});
        System.out.println("chargement de context ...");
        Universite universite1 = context.getBean("UniversiteBejaia", Universite.class);
        Set<Faculte> faculteSet = universite1.getFaculties();
        System.out.println("liste des facultés : ");
        faculteSet.stream().map(faculte -> faculte.getNom_faculte()).forEach(System.out::println);
        System.out.println("liste des departements  par faculté : ");
        faculteSet.stream()

                  .map((faculte) -> {
                    System.out.println("la liste des departements pour la faculté "+faculte.getNom_faculte());
                    return faculte.getDepartements();})
                  .flatMap(Collection::stream)
                  .map(departement -> departement.getNom_departement())
                  .forEach(System.out::println);
        System.out.println("----------------------------------");
        System.out.println("liste  de ensignants par departement ");
        faculteSet.stream()
                .map((faculte) -> {
                    System.out.println("la liste des departements pour la faculté  "+faculte.getNom_faculte()+" filtre ensignants ");
                    return faculte.getDepartements();})
                .flatMap(Collection::stream)
                .map(Departement::getEnsignants)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream).map(ensignant -> ensignant.getNom() + "  "+ensignant.getPrenom())
                //.flatMap(Collection::stream)
                //.map(ensignant -> ensignant.getNom()).filter(Objects::nonNull)
                .forEach(System.out::println);
        System.out.println("le recteur : "+universite1.getRecteur());
        System.out.println("fin chargement de context ");
        System.out.println("detruire le context "+context.getDisplayName());
        ((ClassPathXmlApplicationContext)context).close();

    }

}
