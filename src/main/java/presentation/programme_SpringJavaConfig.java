package presentation;

import Configuration.SpringJavaConfiguration;
import Configuration.UniversiteProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import service_javaConfig.Departement;
import service_javaConfig.Faculte;
import service_javaConfig.Universite;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;


public class programme_SpringJavaConfig {
    public static void main(String[] args) {
        System.out.println("chargement du contexte de la classe de configuration  ...");
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringJavaConfiguration.class);
        Universite universite = applicationContext.getBean(Universite.class);
        //UniversiteProperties universiteProperties = applicationContext.getBean(UniversiteProperties.class);
       // Departement departement = applicationContext.getBean("informatique",Departement.class);
        //System.out.println(universiteProperties);
        System.out.println("--------liste des facultés --------- ");
        Set<Faculte> faculteSet = universite.getFaculties();
        faculteSet.stream().map(Faculte::getNom_faculte).forEach(System.out::println);
        System.out.println("l------iste des departements  par faculté ---------");
        faculteSet.stream()
                .map(faculte -> {
                    System.out.println("liste des departement de la "+faculte.getNom_faculte());
                    return faculte.getDepartements();})
                .flatMap(Collection::stream)
                .map(Departement::getNom_departement)
                .forEach(System.out::println);
        System.out.println("liste des enseignants  ");
        faculteSet.stream()
                .map(Faculte::getDepartements)
                .flatMap(Collection::stream)
                .map(Departement::getEnsignants)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream )
                .map(ensignant -> ensignant.getNom()+"  "+ensignant.getPrenom())
                .forEach(System.out::println);
        System.out.println("-----  doyen de faculté -----");
        faculteSet.stream()
                .map(faculte -> {
                    System.out.println(faculte.getNom_faculte());
                    return faculte.getDoyen() ;})
                .forEach(System.out::println);
    }
}
