package service_javaConfig;

public class Ensignant extends Personne {

    private  int AnneeExperience;


    public int getAnneeExperience() {
        return AnneeExperience;
    }

    public void setAnneeExperience(int anneeExperience) {
        AnneeExperience = anneeExperience;
    }

    @Override
    public String toString() {
        return "Ensignant{" +
                ", AnneeExperience=" + AnneeExperience +
                "} " + super.toString();
    }
}
