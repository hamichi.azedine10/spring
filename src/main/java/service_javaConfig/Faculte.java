package service_javaConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Set;

public class Faculte {

    private  String nom_faculte;
    private Set<Departement> departements;
    public Personne doyen ;

    public String getNom_faculte() {
        return nom_faculte;
    }

    public void setNom_faculte(String nom_faculte) {
        this.nom_faculte = nom_faculte;
    }

    public Set<Departement> getDepartements() {
        return departements;
    }


    public void setDepartements(Set<Departement> departements) {
        this.departements = departements;
    }

    public Personne getDoyen() {
        return doyen;
    }

    public void setDoyen(Doyen doyen) {
        this.doyen = doyen;
    }

    @Override
    public String toString() {
        return "Faculte{" +
                "nom_faculte='" + nom_faculte + '\'' +
                ", departements=" + departements +
                ", doyen=" + doyen +
                '}';
    }
}
