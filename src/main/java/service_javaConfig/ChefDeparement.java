package service_javaConfig;

import java.util.Date;

public class ChefDeparement extends Personne {
    private Date datenomination;

    public Date getDatenomination() {
        return datenomination;
    }

    public void setDatenomination(Date datenomination) {
        this.datenomination = datenomination;
    }

    @Override
    public String toString() {
        return "ChefDeparement{" +
                "datenomination=" + datenomination +
                "} " + super.toString();
    }
}
