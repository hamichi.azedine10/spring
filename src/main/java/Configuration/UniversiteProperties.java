package Configuration;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public class UniversiteProperties {

   // unversite
    @Value("${service.UniversiteBejaia.nom}")
    private String service_UniversiteBejaia_nom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.UniversiteBejaia.date_creation}')}")
    private Date service_UniversiteBejaia_date_creation;

    // facultes
    @Value("${service.Faculte_medcine.nom_faculte}")
    private  String service_Faculte_medcine_nom_faculte;
    @Value("${service.Faculte_technologie.nom_faculte}")
    private  String  service_Faculte_technologie_nom_faculte;
    @Value("${service.Faculte_economie.nom_faculte}")
    private  String service_Faculte_economie_nom_faculte;
    @Value("${service.Facultie_biologie.nom_faculte}")
    private String  service_Facultie_biologie_nom_faculte;

    // Departements
    @Value("${service.medcine_dentaire.nom_departement}")
    private String service_medcine_dentaire_nom_departement;
    @Value("#{new Integer('${service.medcine_dentaire.nombreEtudiants}')}")
    private int service_medcine_dentaire_nombreEtudiants;
    @Value("${service.medcine_generale.nom_departement}")
    private  String service_medcine_generale_nom_departement;
    @Value("#{new Integer('${service.medcine_generale.nombreEtudiants}')}")
    private int service_medcine_generale_nombreEtudiants;
    @Value("${service.medcine_chirurgicale.nom_departement}")
    private  String service_medcine_chirurgicale_nom_departement;
    @Value("#{new Integer('${service.medcine_chirurgicale.nombreEtudiants}')}")
    private int service_medcine_chirurgicale_nombreEtudiants;
    @Value("${service.informatique.nom_departement}")
    private String service_informatique_nom_departement;
    @Value("#{new Integer('${service.informatique.nombreEtudiants}')}")
    private int service_informatique_nombreEtudiants;
    @Value("${service.mathemathique.nom_departement}")
    private String service_mathemathique_nom_departement;
    @Value("#{new Integer('${service.mathemathique.nombreEtudiants}')}")
    private int service_mathemathique_nombreEtudiants;
    @Value("${service.genieElectrique.nom_departement}")
    private String service_genieElectrique_nom_departement;
    @Value("#{new Integer('${service.genieElectrique.nombreEtudiants}')}")
    private int service_genieElectrique_nombreEtudiants;
    @Value("${service.sciencecomerciale.nom_departement}")
    private String service_sciencecomerciale_nom_departement;
    @Value("#{new Integer('${service.sciencecomerciale.nombreEtudiants}')}")
    private int service_sciencecomerciale_nombreEtudiants;
    @Value("${service.sciencegestion.nom_departement}")
    private String service_sciencegestion_nom_departement;
    @Value("#{new Integer('${service.sciencegestion.nombreEtudiants}')}")
    private int service_sciencegestion_nombreEtudiants;
    @Value("${service.scienceeconomique.nom_departement}")
    private String service_scienceeconomique_nom_departement;
    @Value("#{new Integer('${service.scienceeconomique.nombreEtudiants}')}")
    private int service_scienceeconomique_nombreEtudiants;
    @Value("${service.sciencefinanciereEtcomptabilite.nom_departement}")
    private String service_sciencefinanciereEtcomptabilite_nom_departement;
    @Value("#{new Integer('${service.sciencefinanciereEtcomptabilite.nombreEtudiants}')}")
    private int service_sciencefinanciereEtcomptabilite_nombreEtudiants;
    @Value("${service.sciencesBiologiquesdeEnvironnement.nom_departement}")
    private String service_sciencesBiologiquesdeEnvironnement_nom_departement;
    @Value("#{new Integer('${service.sciencesBiologiquesdeEnvironnement.nombreEtudiants}')}")
    private int service_sciencesBiologiquesdeEnvironnement_nombreEtudiants;
    @Value("${service.biologiePhysicoChimique.nom_departement}")
    private String service_biologiePhysicoChimique_nom_departement;
    @Value("#{new Integer('${service.biologiePhysicoChimique.nombreEtudiants}')}")
    private int service_biologiePhysicoChimique_nombreEtudiants;
    @Value("${service.sciencesAlimentaires.nom_departement}")
    private String service_sciencesAlimentaires_nom_departement;
    @Value("#{new Integer('${service.sciencesAlimentaires.nombreEtudiants}')}")
    private int service_sciencesAlimentaires_nombreEtudiants;
    @Value("${service.Microbiologie.nom_departement}")
    private String  service_Microbiologie_nom_departement;
    @Value("#{new Integer('${service.Microbiologie.nombreEtudiants}')}")
    private int service_Microbiologie_nombreEtudiants;
    // Administrateurs

    // recteur
    @Value("${service.recteur.nom}")
    private String service_recteur_nom;
    @Value("${service.recteur.prenom}")
    private String service_recteur_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.recteur.Datenaissance}')}")
    private Date service_recteur_dateNaissance;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.recteur.Datenomination}')}")
    private Date service_recteur_datenomination;

   // doyens
    @Value("${service.doyenTechnologie.nom}")
    private String service_doyenTechnologie_nom;
    @Value("${service.doyenTechnologie.prenom}")
    private String service_doyenTechnologie_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyenTechnologie.date_naissance}')}")
    private Date service_doyentechnologie_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyenTechnologie.dateoccupationposte}')}")
    private  Date service_doyentechnologie_dateoccupationposte;

    @Value("${service.doyenBiologie.nom}")
    private String service_doyenBiologie_nom;
    @Value("${service.doyenBiologie.prenom}")
    private String service_doyenBiologie_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyenBiologie.date_naissance}')}")
    private Date service_doyenBiologie_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyenBiologie.dateoccupationposte}')}")
    private  Date service_doyenBiologie_dateoccupationposte;

    @Value("${service.doyenmedicine.nom}")
    private String service_doyenmedicine_nom;
    @Value("${service.doyenmedicine.prenom}")
    private String service_doyenmedicine_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyenmedicine.date_naissance}')}")
    private Date service_doyenmedicine_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyenmedicine.dateoccupationposte}')}")
    private  Date service_doyenmedicine_dateoccupationposte;

    @Value("${service.doyeneconomie.nom}")
    private String service_doyeneconomie_nom;
    @Value("${service.doyeneconomie.prenom}")
    private String service_doyeneconomie_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyeneconomie.date_naissance}')}")
    private Date service_doyeneconomie_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.doyeneconomie.dateoccupationposte}')}")
    private  Date service_doyeneconomie_dateoccupationposte;

    // Chefs departements

    @Value("${service.chefdepmedcinedentaire.nom}")
    private String service_chefdepmedcinedentaire_nom;
    @Value("${service.chefdepmedcinedentaire.prenom}")
    private String service_chefdepmedcinedentaire_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmedcinedentaire.date_naissance}')}")
    private Date service_chefdepmedcinedentaire_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmedcinedentaire.datenomination}')}")
    private  Date service_chefdepmedcinedentaire_datenomination;

    @Value("${service.chefdepmedcinegenerale.nom}")
    private String service_chefdepmedcinegenerale_nom;
    @Value("${service.chefdepmedcinegenerale.prenom}")
    private String service_chefdepmedcinegenerale_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmedcinegenerale.date_naissance}')}")
    private Date service_chefdepmedcinegenerale_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmedcinegenerale.datenomination}')}")
    private  Date service_chefdepmedcinegenerale_datenomination;

    @Value("${service.chefdepmedcinechirurgical.nom}")
    private String service_chefdepmedcinechirurgical_nom;
    @Value("${service.chefdepmedcinechirurgical.prenom}")
    private String service_chefdepmedcinechirurgical_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmedcinechirurgical.date_naissance}')}")
    private Date service_chefdepmedcinechirurgical_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmedcinechirurgical.datenomination}')}")
    private  Date service_chefdepmedcinechirurgical_datenomination;

    @Value("${service.chefdepinformatique.nom}")
    private String service_chefdepinformatique_nom;
    @Value("${service.chefdepinformatique.prenom}")
    private String service_chefdepinformatique_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepinformatique.date_naissance}')}")
    private Date service_chefdepinformatique_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepinformatique.datenomination}')}")
    private  Date service_chefdepinformatique_datenomination;

    @Value("${service.chefdepmathemathique.nom}")
    private String service_chefdepmathemathique_nom;
    @Value("${service.chefdepmathemathique.prenom}")
    private String service_chefdepmathemathique_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmathemathique.date_naissance}')}")
    private Date service_chefdepmathemathique_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepmathemathique.datenomination}')}")
    private  Date service_chefdepmathemathique_datenomination;

    @Value("${service.chefdepgenieElectrique.nom}")
    private String service_chefdepgenieElectrique_nom;
    @Value("${service.chefdepgenieElectrique.prenom}")
    private String service_chefdepgenieElectrique_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepgenieElectrique.date_naissance}')}")
    private Date service_chefdepgenieElectrique_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepgenieElectrique.datenomination}')}")
    private  Date service_chefdepgenieElectrique_datenomination;

    @Value("${service.chefdepsciencecomerciale.nom}")
    private String service_chefdepsciencecomerciale_nom;
    @Value("${service.chefdepsciencecomerciale.prenom}")
    private String service_chefdepsciencecomerciale_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencecomerciale.date_naissance}')}")
    private Date service_chefdepsciencecomerciale_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencecomerciale.datenomination}')}")
    private  Date service_chefdepsciencecomerciale_datenomination;

    @Value("${service.chefdepsciencegestion.nom}")
    private String service_chefdepsciencegestion_nom;
    @Value("${service.chefdepsciencegestion.prenom}")
    private String service_chefdepsciencegestion_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencegestion.date_naissance}')}")
    private Date service_chefdepsciencegestion_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencegestion.datenomination}')}")
    private  Date service_chefdepsciencegestion_datenomination;

    @Value("${service.chefdepscienceeconomique.nom}")
    private String service_chefdepscienceeconomique_nom;
    @Value("${service.chefdepscienceeconomique.prenom}")
    private String service_chefdepscienceeconomique_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepscienceeconomique.date_naissance}')}")
    private Date service_chefdepscienceeconomique_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepscienceeconomique.datenomination}')}")
    private Date service_chefdepscienceeconomique_datenomination;

    @Value("${service.chefdepsciencefinanciereEtcomptabilite.nom}")
    private String service_chefdepsciencefinanciereEtcomptabilite_nom;
    @Value("${service.chefdepsciencefinanciereEtcomptabilite.prenom}")
    private String service_chefdepsciencefinanciereEtcomptabilite_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencefinanciereEtcomptabilite.date_naissance}')}")
    private Date service_chefdepsciencefinanciereEtcomptabilite_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencefinanciereEtcomptabilite.datenomination}')}")
    private Date service_chefdepsciencefinanciereEtcomptabilite_datenomination;

    @Value("${service.chefdepsciencesBiologiquesdeEnvironnement.nom}")
    private String service_chefdepsciencesBiologiquesdeEnvironnement_nom;
    @Value("${service.chefdepsciencesBiologiquesdeEnvironnement.prenom}")
    private String service_chefdepsciencesBiologiquesdeEnvironnement_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencesBiologiquesdeEnvironnement.date_naissance}')}")
    private Date service_chefdepsciencesBiologiquesdeEnvironnement_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencesBiologiquesdeEnvironnement.datenomination}')}")
    private  Date service_chefdepsciencesBiologiquesdeEnvironnement_datenomination;

    @Value("${service.chefdepbiologiePhysicoChimique.nom}")
    private String service_chefdepbiologiePhysicoChimique_nom;
    @Value("${service.chefdepbiologiePhysicoChimique.prenom}")
    private String service_chefdepbiologiePhysicoChimique_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepbiologiePhysicoChimique.date_naissance}')}")
    private Date service_chefdepbiologiePhysicoChimique_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepbiologiePhysicoChimique.datenomination}')}")
    private  Date service_chefdepbiologiePhysicoChimique_datenomination;

    @Value("${service.chefdepsciencesAlimentaires.nom}")
    private String service_chefdepsciencesAlimentaires_nom;
    @Value("${service.chefdepsciencesAlimentaires.prenom}")
    private String service_chefdepsciencesAlimentaires_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencesAlimentaires.date_naissance}')}")
    private Date service_chefdepsciencesAlimentaires_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepsciencesAlimentaires.datenomination}')}")
    private  Date service_chefdepsciencesAlimentaires_datenomination;

    @Value("${service.chefdepMicrobiologie.nom}")
    private String service_chefdepMicrobiologie_nom;
    @Value("${service.chefdepMicrobiologie.prenom}")
    private String service_chefdepMicrobiologie_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepMicrobiologie.date_naissance}')}")
    private Date service_chefdepMicrobiologie_date_nessaince;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.chefdepMicrobiologie.datenomination}')}")
    private  Date service_chefdepMicrobiologie_datenomination;

    @Value("${service.ensmathapplique.nom}")
    private String service_ensmathapplique_nom;
    @Value("${service.ensmathapplique.prenom}")
    private String service_ensmathapplique_prenom;
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.ensmathapplique.date_naissance}')}")
    private Date service_ensmathapplique_date_naissance;
    @Value("#{new Integer('${service.ensmathapplique.AnneeExperience}')}")
    private int  service_ensmathapplique_AnneeExperience;//=ensmathapplique_AnneeExperiencetemp;
    @Value("${service.ensdevloppementjava.nom}")
    private String service_ensdevloppementjava_nom;
    @Value("${service.ensdevloppementjava.prenom}")
    private String service_ensdevloppementjava_prenom;
    @Value("#{new Integer('${service.ensdevloppementjava.AnneeExperience}')}")
    //private String ensdevloppementjava_AnneeExperiencetemp;
   // int i = Integer.parseInt(ensdevloppementjava_AnneeExperiencetemp);
    private  int service_ensdevloppementjava_AnneeExperience;//=Integer.parseInt(ensdevloppementjava_AnneeExperiencetemp);
    @Value("#{new java.text.SimpleDateFormat('${aDateFormat}').parse('${service.ensdevloppementjava.date_naissance}')}")
    private  Date service_ensdevloppementjava_date_naissance;
    // getters
    public String getService_UniversiteBejaia_nom() {
        return service_UniversiteBejaia_nom;
    }

    public Date getService_UniversiteBejaia_date_creation() {
        return service_UniversiteBejaia_date_creation;
    }

    public String getService_Faculte_medcine_nom_faculte() {
        return service_Faculte_medcine_nom_faculte;
    }

    public String getService_Faculte_technologie_nom_faculte() {
        return service_Faculte_technologie_nom_faculte;
    }

    public String getService_Faculte_economie_nom_faculte() {
        return service_Faculte_economie_nom_faculte;
    }

    public String getService_Facultie_biologie_nom_faculte() {
        return service_Facultie_biologie_nom_faculte;
    }

    public String getService_medcine_dentaire_nom_departement() {
        return service_medcine_dentaire_nom_departement;
    }

    public String getService_medcine_generale_nom_departement() {
        return service_medcine_generale_nom_departement;
    }

    public String getService_medcine_chirurgicale_nom_departement() {
        return service_medcine_chirurgicale_nom_departement;
    }

    public String getService_informatique_nom_departement() {
        return service_informatique_nom_departement;
    }

    public String getService_mathemathique_nom_departement() {
        return service_mathemathique_nom_departement;
    }

    public String getService_genieElectrique_nom_departement() {
        return service_genieElectrique_nom_departement;
    }

    public String getService_sciencecomerciale_nom_departement() {
        return service_sciencecomerciale_nom_departement;
    }

    public String getService_sciencegestion_nom_departement() {
        return service_sciencegestion_nom_departement;
    }

    public String getService_scienceeconomique_nom_departement() {
        return service_scienceeconomique_nom_departement;
    }

    public String getService_sciencefinanciereEtcomptabilite_nom_departement() {
        return service_sciencefinanciereEtcomptabilite_nom_departement;
    }

    public String getService_sciencesBiologiquesdeEnvironnement_nom_departement() {
        return service_sciencesBiologiquesdeEnvironnement_nom_departement;
    }

    public String getService_biologiePhysicoChimique_nom_departement() {
        return service_biologiePhysicoChimique_nom_departement;
    }

    public String getService_sciencesAlimentaires_nom_departement() {
        return service_sciencesAlimentaires_nom_departement;
    }

    public String getService_Microbiologie_nom_departement() {
        return service_Microbiologie_nom_departement;
    }

    public String getService_recteur_nom() {
        return service_recteur_nom;
    }

    public String getService_recteur_prenom() {
        return service_recteur_prenom;
    }

    public Date getService_recteur_dateNaissance() {
        return service_recteur_dateNaissance;
    }

    public Date getService_recteur_datenomination() {
        return service_recteur_datenomination;
    }

    public String getService_doyenTechnologie_nom() {
        return service_doyenTechnologie_nom;
    }

    public String getService_doyenTechnologie_prenom() {
        return service_doyenTechnologie_prenom;
    }

    public Date getService_doyentechnologie_date_nessaince() {
        return service_doyentechnologie_date_nessaince;
    }

    public Date getService_doyentechnologie_dateoccupationposte() {
        return service_doyentechnologie_dateoccupationposte;
    }

    public String getService_doyenBiologie_nom() {
        return service_doyenBiologie_nom;
    }

    public String getService_doyenBiologie_prenom() {
        return service_doyenBiologie_prenom;
    }

    public Date getService_doyenBiologie_date_nessaince() {
        return service_doyenBiologie_date_nessaince;
    }

    public Date getService_doyenBiologie_dateoccupationposte() {
        return service_doyenBiologie_dateoccupationposte;
    }

    public String getService_doyenmedicine_nom() {
        return service_doyenmedicine_nom;
    }

    public String getService_doyenmedicine_prenom() {
        return service_doyenmedicine_prenom;
    }

    public Date getService_doyenmedicine_date_nessaince() {
        return service_doyenmedicine_date_nessaince;
    }

    public Date getService_doyenmedicine_dateoccupationposte() {
        return service_doyenmedicine_dateoccupationposte;
    }

    public String getService_doyeneconomie_nom() {
        return service_doyeneconomie_nom;
    }

    public String getService_doyeneconomie_prenom() {
        return service_doyeneconomie_prenom;
    }

    public Date getService_doyeneconomie_date_nessaince() {
        return service_doyeneconomie_date_nessaince;
    }

    public Date getService_doyeneconomie_dateoccupationposte() {
        return service_doyeneconomie_dateoccupationposte;
    }

    public String getService_chefdepmedcinedentaire_nom() {
        return service_chefdepmedcinedentaire_nom;
    }

    public String getService_chefdepmedcinedentaire_prenom() {
        return service_chefdepmedcinedentaire_prenom;
    }

    public Date getService_chefdepmedcinedentaire_date_nessaince() {
        return service_chefdepmedcinedentaire_date_nessaince;
    }

    public Date getService_chefdepmedcinedentaire_datenomination() {
        return service_chefdepmedcinedentaire_datenomination;
    }

    public String getService_chefdepmedcinegenerale_nom() {
        return service_chefdepmedcinegenerale_nom;
    }

    public String getService_chefdepmedcinegenerale_prenom() {
        return service_chefdepmedcinegenerale_prenom;
    }

    public Date getService_chefdepmedcinegenerale_date_nessaince() {
        return service_chefdepmedcinegenerale_date_nessaince;
    }

    public Date getService_chefdepmedcinegenerale_datenomination() {
        return service_chefdepmedcinegenerale_datenomination;
    }

    public String getService_chefdepmedcinechirurgical_nom() {
        return service_chefdepmedcinechirurgical_nom;
    }

    public String getService_chefdepmedcinechirurgical_prenom() {
        return service_chefdepmedcinechirurgical_prenom;
    }

    public Date getService_chefdepmedcinechirurgical_date_nessaince() {
        return service_chefdepmedcinechirurgical_date_nessaince;
    }

    public Date getService_chefdepmedcinechirurgical_datenomination() {
        return service_chefdepmedcinechirurgical_datenomination;
    }

    public String getService_chefdepinformatique_nom() {
        return service_chefdepinformatique_nom;
    }

    public String getService_chefdepinformatique_prenom() {
        return service_chefdepinformatique_prenom;
    }

    public Date getService_chefdepinformatique_date_nessaince() {
        return service_chefdepinformatique_date_nessaince;
    }

    public Date getService_chefdepinformatique_datenomination() {
        return service_chefdepinformatique_datenomination;
    }

    public String getService_chefdepmathemathique_nom() {
        return service_chefdepmathemathique_nom;
    }

    public String getService_chefdepmathemathique_prenom() {
        return service_chefdepmathemathique_prenom;
    }

    public Date getService_chefdepmathemathique_date_nessaince() {
        return service_chefdepmathemathique_date_nessaince;
    }

    public Date getService_chefdepmathemathique_datenomination() {
        return service_chefdepmathemathique_datenomination;
    }

    public String getService_chefdepgenieElectrique_nom() {
        return service_chefdepgenieElectrique_nom;
    }

    public String getService_chefdepgenieElectrique_prenom() {
        return service_chefdepgenieElectrique_prenom;
    }

    public Date getService_chefdepgenieElectrique_date_nessaince() {
        return service_chefdepgenieElectrique_date_nessaince;
    }

    public Date getService_chefdepgenieElectrique_datenomination() {
        return service_chefdepgenieElectrique_datenomination;
    }

    public String getService_chefdepsciencecomerciale_nom() {
        return service_chefdepsciencecomerciale_nom;
    }

    public String getService_chefdepsciencecomerciale_prenom() {
        return service_chefdepsciencecomerciale_prenom;
    }

    public Date getService_chefdepsciencecomerciale_date_nessaince() {
        return service_chefdepsciencecomerciale_date_nessaince;
    }

    public Date getService_chefdepsciencecomerciale_datenomination() {
        return service_chefdepsciencecomerciale_datenomination;
    }

    public String getService_chefdepsciencegestion_nom() {
        return service_chefdepsciencegestion_nom;
    }

    public String getService_chefdepsciencegestion_prenom() {
        return service_chefdepsciencegestion_prenom;
    }

    public Date getService_chefdepsciencegestion_date_nessaince() {
        return service_chefdepsciencegestion_date_nessaince;
    }

    public Date getService_chefdepsciencegestion_datenomination() {
        return service_chefdepsciencegestion_datenomination;
    }

    public String getService_chefdepscienceeconomique_nom() {
        return service_chefdepscienceeconomique_nom;
    }

    public String getService_chefdepscienceeconomique_prenom() {
        return service_chefdepscienceeconomique_prenom;
    }

    public Date getService_chefdepscienceeconomique_date_nessaince() {
        return service_chefdepscienceeconomique_date_nessaince;
    }

    public Date getService_chefdepscienceeconomique_datenomination() {
        return service_chefdepscienceeconomique_datenomination;
    }

    public String getService_chefdepsciencefinanciereEtcomptabilite_nom() {
        return service_chefdepsciencefinanciereEtcomptabilite_nom;
    }

    public String getService_chefdepsciencefinanciereEtcomptabilite_prenom() {
        return service_chefdepsciencefinanciereEtcomptabilite_prenom;
    }

    public Date getService_chefdepsciencefinanciereEtcomptabilite_date_nessaince() {
        return service_chefdepsciencefinanciereEtcomptabilite_date_nessaince;
    }

    public Date getService_chefdepsciencefinanciereEtcomptabilite_datenomination() {
        return service_chefdepsciencefinanciereEtcomptabilite_datenomination;
    }

    public String getService_chefdepsciencesBiologiquesdeEnvironnement_nom() {
        return service_chefdepsciencesBiologiquesdeEnvironnement_nom;
    }

    public String getService_chefdepsciencesBiologiquesdeEnvironnement_prenom() {
        return service_chefdepsciencesBiologiquesdeEnvironnement_prenom;
    }

    public Date getService_chefdepsciencesBiologiquesdeEnvironnement_date_nessaince() {
        return service_chefdepsciencesBiologiquesdeEnvironnement_date_nessaince;
    }

    public Date getService_chefdepsciencesBiologiquesdeEnvironnement_datenomination() {
        return service_chefdepsciencesBiologiquesdeEnvironnement_datenomination;
    }

    public String getService_chefdepbiologiePhysicoChimique_nom() {
        return service_chefdepbiologiePhysicoChimique_nom;
    }

    public String getService_chefdepbiologiePhysicoChimique_prenom() {
        return service_chefdepbiologiePhysicoChimique_prenom;
    }

    public Date getService_chefdepbiologiePhysicoChimique_date_nessaince() {
        return service_chefdepbiologiePhysicoChimique_date_nessaince;
    }

    public Date getService_chefdepbiologiePhysicoChimique_datenomination() {
        return service_chefdepbiologiePhysicoChimique_datenomination;
    }

    public String getService_chefdepsciencesAlimentaires_nom() {
        return service_chefdepsciencesAlimentaires_nom;
    }

    public String getService_chefdepsciencesAlimentaires_prenom() {
        return service_chefdepsciencesAlimentaires_prenom;
    }

    public Date getService_chefdepsciencesAlimentaires_date_nessaince() {
        return service_chefdepsciencesAlimentaires_date_nessaince;
    }

    public Date getService_chefdepsciencesAlimentaires_datenomination() {
        return service_chefdepsciencesAlimentaires_datenomination;
    }

    public String getService_chefdepMicrobiologie_nom() {
        return service_chefdepMicrobiologie_nom;
    }

    public String getService_chefdepMicrobiologie_prenom() {
        return service_chefdepMicrobiologie_prenom;
    }

    public Date getService_chefdepMicrobiologie_date_nessaince() {
        return service_chefdepMicrobiologie_date_nessaince;
    }

    public Date getService_chefdepMicrobiologie_datenomination() {
        return service_chefdepMicrobiologie_datenomination;
    }

    public String getService_ensmathapplique_nom() {
        return service_ensmathapplique_nom;
    }

    public String getService_ensmathapplique_prenom() {
        return service_ensmathapplique_prenom;
    }

    public Date getService_ensmathapplique_date_naissance() {
        return service_ensmathapplique_date_naissance;
    }

    public int getService_ensmathapplique_AnneeExperience() {
        return service_ensmathapplique_AnneeExperience;
    }

    public String getService_ensdevloppementjava_nom() {
        return service_ensdevloppementjava_nom;
    }

    public String getService_ensdevloppementjava_prenom() {
        return service_ensdevloppementjava_prenom;
    }

    public int getService_ensdevloppementjava_AnneeExperience() {
        return service_ensdevloppementjava_AnneeExperience;
    }

    public int getService_medcine_dentaire_nombreEtudiants() {
        return service_medcine_dentaire_nombreEtudiants;
    }

    public int getService_medcine_generale_nombreEtudiants() {
        return service_medcine_generale_nombreEtudiants;
    }

    public int getService_medcine_chirurgicale_nombreEtudiants() {
        return service_medcine_chirurgicale_nombreEtudiants;
    }

    public int getService_informatique_nombreEtudiants() {
        return service_informatique_nombreEtudiants;
    }

    public int getService_mathemathique_nombreEtudiants() {
        return service_mathemathique_nombreEtudiants;
    }

    public int getService_genieElectrique_nombreEtudiants() {
        return service_genieElectrique_nombreEtudiants;
    }

    public int getService_sciencecomerciale_nombreEtudiants() {
        return service_sciencecomerciale_nombreEtudiants;
    }

    public int getService_sciencegestion_nombreEtudiants() {
        return service_sciencegestion_nombreEtudiants;
    }

    public int getService_scienceeconomique_nombreEtudiants() {
        return service_scienceeconomique_nombreEtudiants;
    }

    public int getService_sciencefinanciereEtcomptabilite_nombreEtudiants() {
        return service_sciencefinanciereEtcomptabilite_nombreEtudiants;
    }

    public int getService_sciencesBiologiquesdeEnvironnement_nombreEtudiants() {
        return service_sciencesBiologiquesdeEnvironnement_nombreEtudiants;
    }

    public int getService_biologiePhysicoChimique_nombreEtudiants() {
        return service_biologiePhysicoChimique_nombreEtudiants;
    }

    public int getService_sciencesAlimentaires_nombreEtudiants() {
        return service_sciencesAlimentaires_nombreEtudiants;
    }

    public int getService_Microbiologie_nombreEtudiants() {
        return service_Microbiologie_nombreEtudiants;
    }

    public Date getService_ensdevloppementjava_date_naissance() {
        return service_ensdevloppementjava_date_naissance;
    }

    @Override
    public String toString() {
        return "UniversiteProperties{" +
                "service_UniversiteBejaia_nom='" + service_UniversiteBejaia_nom + '\'' +
                ", service_UniversiteBejaia_date_creation=" + service_UniversiteBejaia_date_creation +
                ", service_Faculte_medcine_nom_faculte='" + service_Faculte_medcine_nom_faculte + '\'' +
                ", service_Faculte_technologie_nom_faculte='" + service_Faculte_technologie_nom_faculte + '\'' +
                ", service_Faculte_economie_nom_faculte='" + service_Faculte_economie_nom_faculte + '\'' +
                ", service_Facultie_biologie_nom_faculte='" + service_Facultie_biologie_nom_faculte + '\'' +
                ", service_medcine_dentaire_nom_departement='" + service_medcine_dentaire_nom_departement + '\'' +
                ", service_medcine_dentaire_nombreEtudiants=" + service_medcine_dentaire_nombreEtudiants +
                ", service_medcine_generale_nom_departement='" + service_medcine_generale_nom_departement + '\'' +
                ", service_medcine_generale_nombreEtudiants=" + service_medcine_generale_nombreEtudiants +
                ", service_medcine_chirurgicale_nom_departement='" + service_medcine_chirurgicale_nom_departement + '\'' +
                ", service_medcine_chirurgicale_nombreEtudiants=" + service_medcine_chirurgicale_nombreEtudiants +
                ", service_informatique_nom_departement='" + service_informatique_nom_departement + '\'' +
                ", service_informatique_nombreEtudiants=" + service_informatique_nombreEtudiants +
                ", service_mathemathique_nom_departement='" + service_mathemathique_nom_departement + '\'' +
                ", service_mathemathique_nombreEtudiants=" + service_mathemathique_nombreEtudiants +
                ", service_genieElectrique_nom_departement='" + service_genieElectrique_nom_departement + '\'' +
                ", service_genieElectrique_nombreEtudiants=" + service_genieElectrique_nombreEtudiants +
                ", service_sciencecomerciale_nom_departement='" + service_sciencecomerciale_nom_departement + '\'' +
                ", service_sciencecomerciale_nombreEtudiants=" + service_sciencecomerciale_nombreEtudiants +
                ", service_sciencegestion_nom_departement='" + service_sciencegestion_nom_departement + '\'' +
                ", service_sciencegestion_nombreEtudiants=" + service_sciencegestion_nombreEtudiants +
                ", service_scienceeconomique_nom_departement='" + service_scienceeconomique_nom_departement + '\'' +
                ", service_scienceeconomique_nombreEtudiants=" + service_scienceeconomique_nombreEtudiants +
                ", service_sciencefinanciereEtcomptabilite_nom_departement='" + service_sciencefinanciereEtcomptabilite_nom_departement + '\'' +
                ", service_sciencefinanciereEtcomptabilite_nombreEtudiants=" + service_sciencefinanciereEtcomptabilite_nombreEtudiants +
                ", service_sciencesBiologiquesdeEnvironnement_nom_departement='" + service_sciencesBiologiquesdeEnvironnement_nom_departement + '\'' +
                ", service_sciencesBiologiquesdeEnvironnement_nombreEtudiants=" + service_sciencesBiologiquesdeEnvironnement_nombreEtudiants +
                ", service_biologiePhysicoChimique_nom_departement='" + service_biologiePhysicoChimique_nom_departement + '\'' +
                ", service_biologiePhysicoChimique_nombreEtudiants=" + service_biologiePhysicoChimique_nombreEtudiants +
                ", service_sciencesAlimentaires_nom_departement='" + service_sciencesAlimentaires_nom_departement + '\'' +
                ", service_sciencesAlimentaires_nombreEtudiants=" + service_sciencesAlimentaires_nombreEtudiants +
                ", service_Microbiologie_nom_departement='" + service_Microbiologie_nom_departement + '\'' +
                ", service_Microbiologie_nombreEtudiants=" + service_Microbiologie_nombreEtudiants +
                ", service_recteur_nom='" + service_recteur_nom + '\'' +
                ", service_recteur_prenom='" + service_recteur_prenom + '\'' +
                ", service_recteur_dateNaissance=" + service_recteur_dateNaissance +
                ", service_recteur_datenomination=" + service_recteur_datenomination +
                ", service_doyenTechnologie_nom='" + service_doyenTechnologie_nom + '\'' +
                ", service_doyenTechnologie_prenom='" + service_doyenTechnologie_prenom + '\'' +
                ", service_doyentechnologie_date_nessaince=" + service_doyentechnologie_date_nessaince +
                ", service_doyentechnologie_dateoccupationposte=" + service_doyentechnologie_dateoccupationposte +
                ", service_doyenBiologie_nom='" + service_doyenBiologie_nom + '\'' +
                ", service_doyenBiologie_prenom='" + service_doyenBiologie_prenom + '\'' +
                ", service_doyenBiologie_date_nessaince=" + service_doyenBiologie_date_nessaince +
                ", service_doyenBiologie_dateoccupationposte=" + service_doyenBiologie_dateoccupationposte +
                ", service_doyenmedicine_nom='" + service_doyenmedicine_nom + '\'' +
                ", service_doyenmedicine_prenom='" + service_doyenmedicine_prenom + '\'' +
                ", service_doyenmedicine_date_nessaince=" + service_doyenmedicine_date_nessaince +
                ", service_doyenmedicine_dateoccupationposte=" + service_doyenmedicine_dateoccupationposte +
                ", service_doyeneconomie_nom='" + service_doyeneconomie_nom + '\'' +
                ", service_doyeneconomie_prenom='" + service_doyeneconomie_prenom + '\'' +
                ", service_doyeneconomie_date_nessaince=" + service_doyeneconomie_date_nessaince +
                ", service_doyeneconomie_dateoccupationposte=" + service_doyeneconomie_dateoccupationposte +
                ", service_chefdepmedcinedentaire_nom='" + service_chefdepmedcinedentaire_nom + '\'' +
                ", service_chefdepmedcinedentaire_prenom='" + service_chefdepmedcinedentaire_prenom + '\'' +
                ", service_chefdepmedcinedentaire_date_nessaince=" + service_chefdepmedcinedentaire_date_nessaince +
                ", service_chefdepmedcinedentaire_datenomination=" + service_chefdepmedcinedentaire_datenomination +
                ", service_chefdepmedcinegenerale_nom='" + service_chefdepmedcinegenerale_nom + '\'' +
                ", service_chefdepmedcinegenerale_prenom='" + service_chefdepmedcinegenerale_prenom + '\'' +
                ", service_chefdepmedcinegenerale_date_nessaince=" + service_chefdepmedcinegenerale_date_nessaince +
                ", service_chefdepmedcinegenerale_datenomination=" + service_chefdepmedcinegenerale_datenomination +
                ", service_chefdepmedcinechirurgical_nom='" + service_chefdepmedcinechirurgical_nom + '\'' +
                ", service_chefdepmedcinechirurgical_prenom='" + service_chefdepmedcinechirurgical_prenom + '\'' +
                ", service_chefdepmedcinechirurgical_date_nessaince=" + service_chefdepmedcinechirurgical_date_nessaince +
                ", service_chefdepmedcinechirurgical_datenomination=" + service_chefdepmedcinechirurgical_datenomination +
                ", service_chefdepinformatique_nom='" + service_chefdepinformatique_nom + '\'' +
                ", service_chefdepinformatique_prenom='" + service_chefdepinformatique_prenom + '\'' +
                ", service_chefdepinformatique_date_nessaince=" + service_chefdepinformatique_date_nessaince +
                ", service_chefdepinformatique_datenomination=" + service_chefdepinformatique_datenomination +
                ", service_chefdepmathemathique_nom='" + service_chefdepmathemathique_nom + '\'' +
                ", service_chefdepmathemathique_prenom='" + service_chefdepmathemathique_prenom + '\'' +
                ", service_chefdepmathemathique_date_nessaince=" + service_chefdepmathemathique_date_nessaince +
                ", service_chefdepmathemathique_datenomination=" + service_chefdepmathemathique_datenomination +
                ", service_chefdepgenieElectrique_nom='" + service_chefdepgenieElectrique_nom + '\'' +
                ", service_chefdepgenieElectrique_prenom='" + service_chefdepgenieElectrique_prenom + '\'' +
                ", service_chefdepgenieElectrique_date_nessaince=" + service_chefdepgenieElectrique_date_nessaince +
                ", service_chefdepgenieElectrique_datenomination=" + service_chefdepgenieElectrique_datenomination +
                ", service_chefdepsciencecomerciale_nom='" + service_chefdepsciencecomerciale_nom + '\'' +
                ", service_chefdepsciencecomerciale_prenom='" + service_chefdepsciencecomerciale_prenom + '\'' +
                ", service_chefdepsciencecomerciale_date_nessaince=" + service_chefdepsciencecomerciale_date_nessaince +
                ", service_chefdepsciencecomerciale_datenomination=" + service_chefdepsciencecomerciale_datenomination +
                ", service_chefdepsciencegestion_nom='" + service_chefdepsciencegestion_nom + '\'' +
                ", service_chefdepsciencegestion_prenom='" + service_chefdepsciencegestion_prenom + '\'' +
                ", service_chefdepsciencegestion_date_nessaince=" + service_chefdepsciencegestion_date_nessaince +
                ", service_chefdepsciencegestion_datenomination=" + service_chefdepsciencegestion_datenomination +
                ", service_chefdepscienceeconomique_nom='" + service_chefdepscienceeconomique_nom + '\'' +
                ", service_chefdepscienceeconomique_prenom='" + service_chefdepscienceeconomique_prenom + '\'' +
                ", service_chefdepscienceeconomique_date_nessaince=" + service_chefdepscienceeconomique_date_nessaince +
                ", service_chefdepscienceeconomique_datenomination=" + service_chefdepscienceeconomique_datenomination +
                ", service_chefdepsciencefinanciereEtcomptabilite_nom='" + service_chefdepsciencefinanciereEtcomptabilite_nom + '\'' +
                ", service_chefdepsciencefinanciereEtcomptabilite_prenom='" + service_chefdepsciencefinanciereEtcomptabilite_prenom + '\'' +
                ", service_chefdepsciencefinanciereEtcomptabilite_date_nessaince=" + service_chefdepsciencefinanciereEtcomptabilite_date_nessaince +
                ", service_chefdepsciencefinanciereEtcomptabilite_datenomination=" + service_chefdepsciencefinanciereEtcomptabilite_datenomination +
                ", service_chefdepsciencesBiologiquesdeEnvironnement_nom='" + service_chefdepsciencesBiologiquesdeEnvironnement_nom + '\'' +
                ", service_chefdepsciencesBiologiquesdeEnvironnement_prenom='" + service_chefdepsciencesBiologiquesdeEnvironnement_prenom + '\'' +
                ", service_chefdepsciencesBiologiquesdeEnvironnement_date_nessaince=" + service_chefdepsciencesBiologiquesdeEnvironnement_date_nessaince +
                ", service_chefdepsciencesBiologiquesdeEnvironnement_datenomination=" + service_chefdepsciencesBiologiquesdeEnvironnement_datenomination +
                ", service_chefdepbiologiePhysicoChimique_nom='" + service_chefdepbiologiePhysicoChimique_nom + '\'' +
                ", service_chefdepbiologiePhysicoChimique_prenom='" + service_chefdepbiologiePhysicoChimique_prenom + '\'' +
                ", service_chefdepbiologiePhysicoChimique_date_nessaince=" + service_chefdepbiologiePhysicoChimique_date_nessaince +
                ", service_chefdepbiologiePhysicoChimique_datenomination=" + service_chefdepbiologiePhysicoChimique_datenomination +
                ", service_chefdepsciencesAlimentaires_nom='" + service_chefdepsciencesAlimentaires_nom + '\'' +
                ", service_chefdepsciencesAlimentaires_prenom='" + service_chefdepsciencesAlimentaires_prenom + '\'' +
                ", service_chefdepsciencesAlimentaires_date_nessaince=" + service_chefdepsciencesAlimentaires_date_nessaince +
                ", service_chefdepsciencesAlimentaires_datenomination=" + service_chefdepsciencesAlimentaires_datenomination +
                ", service_chefdepMicrobiologie_nom='" + service_chefdepMicrobiologie_nom + '\'' +
                ", service_chefdepMicrobiologie_prenom='" + service_chefdepMicrobiologie_prenom + '\'' +
                ", service_chefdepMicrobiologie_date_nessaince=" + service_chefdepMicrobiologie_date_nessaince +
                ", service_chefdepMicrobiologie_datenomination=" + service_chefdepMicrobiologie_datenomination +
                ", service_ensmathapplique_nom='" + service_ensmathapplique_nom + '\'' +
                ", service_ensmathapplique_prenom='" + service_ensmathapplique_prenom + '\'' +
                ", service_ensmathapplique_date_naissance=" + service_ensmathapplique_date_naissance +
                ", service_ensmathapplique_AnneeExperience=" + service_ensmathapplique_AnneeExperience +
                ", service_ensdevloppementjava_nom='" + service_ensdevloppementjava_nom + '\'' +
                ", service_ensdevloppementjava_prenom='" + service_ensdevloppementjava_prenom + '\'' +
                ", service_ensdevloppementjava_AnneeExperience=" + service_ensdevloppementjava_AnneeExperience +
                '}';
    }
}
