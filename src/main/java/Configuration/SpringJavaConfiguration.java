package Configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import service_javaConfig.*;

import java.util.HashSet;
import java.util.Set;


@Configuration
@PropertySource("classpath:Universite.properties")
public class SpringJavaConfiguration {
    // lire le fichier properties Universite.properties

    @Bean
    @Scope("singleton")
    public UniversiteProperties universiteProperties(){
        return  new UniversiteProperties();
    }

    // universites
    @Bean(initMethod = "init")
    public Universite universite1 (){
        Universite universite = new Universite();
        universite.setDate_creation(universiteProperties().getService_UniversiteBejaia_date_creation());
        universite.setNom(universiteProperties().getService_UniversiteBejaia_nom());
        return  universite;
    }

    // facultés
    @Bean
   // @Qualifier("departementSetOfMedcine")
    public  Faculte Faculte_medcine(){
        Faculte faculte =new  Faculte();
        faculte.setNom_faculte(universiteProperties().getService_Faculte_medcine_nom_faculte());
        faculte.setDepartements(departementSetOfMedcine());
        faculte.setDoyen((Doyen) doyenmedicine());
        return faculte;
    }
    @Bean
    public  Faculte Faculte_technologie(){
        Faculte faculte =new  Faculte();
        faculte.setNom_faculte(universiteProperties().getService_Faculte_technologie_nom_faculte());
        faculte.setDepartements(departementSetOfTechnologie());
        faculte.setDoyen((Doyen)doyenTechnologie());
        return faculte;
    }
    @Bean
    public  Faculte Faculte_economie(){
        Faculte faculte =new  Faculte();
        faculte.setDoyen((Doyen) doyeneconomie());
        faculte.setDepartements(departementSetOfeconomie());
        faculte.setNom_faculte(universiteProperties().getService_Faculte_economie_nom_faculte());
        return faculte;
    }

    @Bean
    public  Faculte Facultie_biologie(){
        Faculte faculte =new  Faculte();
        faculte.setNom_faculte(universiteProperties().getService_Facultie_biologie_nom_faculte());
        faculte.setDepartements(departementSetOfBiologie());
        faculte.setDoyen((Doyen)doyenBiologie());
        return faculte;
    }

    // departments

    @Bean
    public Departement medcine_dentaire(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_medcine_dentaire_nom_departement());
        departement.setChefDeparement((ChefDeparement)chefdepmedcinedentaire());
        departement.setNombreEtudients(universiteProperties().getService_medcine_dentaire_nombreEtudiants());
       // departement.setEnsignants(se);
        return departement;
    }

    @Bean
    public Departement medcine_generale(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_medcine_generale_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepmedcinegenerale());
        departement.setNombreEtudients(universiteProperties().getService_medcine_generale_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement medcine_chirurgicale(){
        Departement departement = new Departement();
        departement.setNom_departement(universiteProperties().getService_medcine_chirurgicale_nom_departement());
        departement.setChefDeparement((ChefDeparement)chefdepmedcinechirurgical());
        departement.setNombreEtudients(universiteProperties().getService_medcine_chirurgicale_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement informatique(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_informatique_nom_departement());
        departement.setChefDeparement((ChefDeparement)chefdepinformatique());
        departement.setNombreEtudients(universiteProperties().getService_informatique_nombreEtudiants());
        departement.setEnsignants(ensignantSetOfInformatique());
        return departement;
    }
    @Bean
    public Departement mathemathique(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_mathemathique_nom_departement());
        departement.setChefDeparement((ChefDeparement)chefdepmathemathique());
        departement.setEnsignants(ensignantSetOfMathemathique());
        departement.setNombreEtudients(universiteProperties().getService_mathemathique_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement genieElectrique(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_genieElectrique_nom_departement());
        departement.setChefDeparement((ChefDeparement)chefdepgenieElectrique());
        departement.setNombreEtudients(universiteProperties().getService_genieElectrique_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement sciencecomerciale(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_sciencecomerciale_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepsciencecomerciale());
        departement.setNombreEtudients(universiteProperties().getService_sciencecomerciale_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement sciencegestion(){
        Departement departement= new  Departement();
        departement.setNom_departement(universiteProperties().getService_sciencegestion_nom_departement());
        departement.setChefDeparement((ChefDeparement)chefdepsciencegestion());
        departement.setNombreEtudients(universiteProperties().getService_sciencegestion_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement scienceeconomique(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_scienceeconomique_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepscienceeconomique());
        departement.setNombreEtudients(universiteProperties().getService_scienceeconomique_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement sciencefinanciereEtcomptabilite(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_sciencefinanciereEtcomptabilite_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepsciencefinanciereEtcomptabilite());
        departement.setNombreEtudients(universiteProperties().getService_sciencefinanciereEtcomptabilite_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement sciencesBiologiquesdeEnvironnement(){
        Departement departement= new Departement();
        departement.setNom_departement(universiteProperties().getService_sciencesBiologiquesdeEnvironnement_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepsciencesBiologiquesdeEnvironnement());
        departement.setNombreEtudients(universiteProperties().getService_sciencesBiologiquesdeEnvironnement_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement biologiePhysicoChimique(){
        Departement departement=new Departement();
        departement.setNom_departement(universiteProperties().getService_biologiePhysicoChimique_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepbiologiePhysicoChimique());
        departement.setNombreEtudients(universiteProperties().getService_biologiePhysicoChimique_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement sciencesAlimentaires(){
        Departement departement = new Departement();
        departement.setNom_departement(universiteProperties().getService_sciencesAlimentaires_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepsciencesAlimentaires());
        departement.setNombreEtudients(universiteProperties().getService_sciencesAlimentaires_nombreEtudiants());
        return departement;
    }
    @Bean
    public Departement Microbiologie(){
        Departement departement = new Departement();
        departement.setNom_departement(universiteProperties().getService_Microbiologie_nom_departement());
        departement.setChefDeparement((ChefDeparement) chefdepMicrobiologie());
        departement.setNombreEtudients(universiteProperties().getService_Microbiologie_nombreEtudiants());
        return  departement;
    }

    // les Administrateurs
    @Bean
    public Recteur recteur() {
    Recteur recteur= new Recteur();
    recteur.setNom(universiteProperties().getService_recteur_nom());
    recteur.setPrenom(universiteProperties().getService_recteur_prenom());
    recteur.setDate_naissance(universiteProperties().getService_recteur_dateNaissance());
    recteur.setDatenomination(universiteProperties().getService_recteur_datenomination());
    return  recteur;
    }
    @Bean
    public Personne doyenTechnologie(){
        Doyen doyen = new Doyen();
        doyen.setNom(universiteProperties().getService_doyenTechnologie_nom());
        doyen.setPrenom(universiteProperties().getService_doyenTechnologie_prenom());
        doyen.setDate_naissance(universiteProperties().getService_doyentechnologie_date_nessaince());
        doyen.setDateocupationposte(universiteProperties().getService_doyentechnologie_dateoccupationposte());
        return doyen;
    }
    @Bean
    public Personne doyenBiologie(){
        Doyen doyen = new Doyen();
        doyen.setNom(universiteProperties().getService_doyenBiologie_nom());
        doyen.setPrenom(universiteProperties().getService_doyenBiologie_prenom());
        doyen.setDate_naissance(universiteProperties().getService_doyenBiologie_date_nessaince());
        doyen.setDateocupationposte(universiteProperties().getService_doyenBiologie_dateoccupationposte());
        return doyen;
    }
    @Bean
    public Personne doyenmedicine(){
        Doyen doyen = new Doyen();
        doyen.setNom(universiteProperties().getService_doyenmedicine_nom());
        doyen.setPrenom(universiteProperties().getService_doyenmedicine_prenom());
        doyen.setDate_naissance(universiteProperties().getService_doyenmedicine_date_nessaince());
        doyen.setDateocupationposte(universiteProperties().getService_doyenmedicine_dateoccupationposte());
        return doyen;
    }
    @Bean
    public Personne doyeneconomie(){
        Doyen doyen = new Doyen();
        doyen.setNom(universiteProperties().getService_doyeneconomie_nom());
        doyen.setPrenom(universiteProperties().getService_doyeneconomie_prenom());
        doyen.setDate_naissance(universiteProperties().getService_doyeneconomie_date_nessaince());
        doyen.setDateocupationposte(universiteProperties().getService_doyeneconomie_dateoccupationposte());
        return doyen;
    }
    @Bean
    public Personne chefdepmedcinedentaire(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepmedcinedentaire_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepmedcinedentaire_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepmedcinedentaire_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepmedcinedentaire_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepmedcinegenerale(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepmedcinegenerale_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepmedcinegenerale_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepmedcinegenerale_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepmedcinegenerale_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepmedcinechirurgical(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepmedcinechirurgical_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepmedcinechirurgical_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepmedcinechirurgical_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepmedcinechirurgical_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepinformatique(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepinformatique_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepinformatique_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepinformatique_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepinformatique_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepmathemathique(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepmathemathique_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepmathemathique_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepmathemathique_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepmathemathique_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepgenieElectrique(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepgenieElectrique_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepgenieElectrique_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepgenieElectrique_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepgenieElectrique_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepsciencecomerciale(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepsciencecomerciale_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepsciencecomerciale_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepsciencecomerciale_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepsciencecomerciale_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepsciencegestion(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepsciencegestion_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepsciencegestion_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepsciencegestion_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepsciencegestion_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepscienceeconomique(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepscienceeconomique_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepscienceeconomique_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepscienceeconomique_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepscienceeconomique_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepsciencefinanciereEtcomptabilite(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepsciencefinanciereEtcomptabilite_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepsciencefinanciereEtcomptabilite_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepsciencefinanciereEtcomptabilite_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepsciencefinanciereEtcomptabilite_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepsciencesBiologiquesdeEnvironnement(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepsciencesBiologiquesdeEnvironnement_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepsciencesBiologiquesdeEnvironnement_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepsciencesBiologiquesdeEnvironnement_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepsciencesBiologiquesdeEnvironnement_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepbiologiePhysicoChimique(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepbiologiePhysicoChimique_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepbiologiePhysicoChimique_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepbiologiePhysicoChimique_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepbiologiePhysicoChimique_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepsciencesAlimentaires(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepsciencesAlimentaires_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepsciencesAlimentaires_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepsciencesAlimentaires_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepsciencesAlimentaires_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne chefdepMicrobiologie(){
        ChefDeparement chefDeparement= new ChefDeparement();
        chefDeparement.setNom(universiteProperties().getService_chefdepMicrobiologie_nom());
        chefDeparement.setPrenom(universiteProperties().getService_chefdepMicrobiologie_prenom());
        chefDeparement.setDate_naissance(universiteProperties().getService_chefdepMicrobiologie_date_nessaince());
        chefDeparement.setDatenomination(universiteProperties().getService_chefdepMicrobiologie_datenomination());
        return chefDeparement;
    }
    @Bean
    public Personne ensmathapplique(){
        Ensignant ensignant= new Ensignant();
        ensignant.setNom(universiteProperties().getService_ensmathapplique_nom());
        ensignant.setPrenom(universiteProperties().getService_ensmathapplique_prenom());
        ensignant.setDate_naissance(universiteProperties().getService_ensmathapplique_date_naissance());
        ensignant.setAnneeExperience(universiteProperties().getService_ensmathapplique_AnneeExperience());
        return ensignant;
    }
    @Bean
    public Personne ensdevloppementjava(){
        Ensignant ensignant= new Ensignant();
        ensignant.setNom(universiteProperties().getService_ensdevloppementjava_nom());
        ensignant.setPrenom(universiteProperties().getService_ensdevloppementjava_prenom());
        ensignant.setDate_naissance(universiteProperties().getService_ensdevloppementjava_date_naissance());
        ensignant.setAnneeExperience(universiteProperties().getService_ensdevloppementjava_AnneeExperience());
        return ensignant;
    }

    // les tableaux
    @Bean("departementSetOfMedcine0")
    public Set<Departement> departementSetOfMedcine(){
        Set<Departement> departementSet = new HashSet<>();
        departementSet.add(sciencecomerciale());
        departementSet.add(medcine_chirurgicale());
        departementSet.add(medcine_dentaire());
        departementSet.add(medcine_generale());
        return  departementSet;
    }
    @Bean("departementSetOfBiologie")
    public  Set<Departement> departementSetOfBiologie(){
        Set<Departement> departementSet = new HashSet<>();
        departementSet.add(Microbiologie());
        departementSet.add(biologiePhysicoChimique());
        departementSet.add(sciencesAlimentaires());
        departementSet.add(sciencesBiologiquesdeEnvironnement());
        return departementSet;
    }
    @Bean("departementSetOfTechnologie")
    public  Set<Departement> departementSetOfTechnologie(){
        Set<Departement> departementSet= new HashSet<>();
        departementSet.add(informatique());
        departementSet.add(mathemathique());
        departementSet.add(genieElectrique());
        return departementSet;
    }
    @Bean("departementSetOfeconomie")
    public  Set<Departement> departementSetOfeconomie(){
        Set<Departement> departementSet= new HashSet<>();
        departementSet.add(sciencecomerciale());
        departementSet.add(scienceeconomique());
        departementSet.add(sciencegestion());
        departementSet.add(sciencefinanciereEtcomptabilite());
        return departementSet;
    }
   @Bean
    public Set<Ensignant> ensignantSetOfInformatique(){
        Set<Ensignant> ensignants=new HashSet<>();
        ensignants.add((Ensignant) ensdevloppementjava());
        return ensignants;
   }

   @Bean
    public Set<Ensignant> ensignantSetOfMathemathique(){
        Set<Ensignant> ensignants=new HashSet<>();
        ensignants.add((Ensignant)ensmathapplique());
        return ensignants;
   }
}
