package service_XmlConfig;

public class Ensignant extends  Personne {
    private String profession ;
    private  int AnneeExperience;

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public int getAnneeExperience() {
        return AnneeExperience;
    }

    public void setAnneeExperience(int anneeExperience) {
        AnneeExperience = anneeExperience;
    }

    @Override
    public String toString() {
        return "Ensignant{" +
                "profession='" + profession + '\'' +
                ", AnneeExperience=" + AnneeExperience +
                "} " + super.toString();
    }
}
