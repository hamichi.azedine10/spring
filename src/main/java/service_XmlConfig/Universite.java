package service_XmlConfig;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Set;

public class Universite {

    private  String nom;
    private  Date date_creation;

    private Set<Faculte> faculties;

    private  Personne recteur;

    public  void init() {
        System.out.println("init method for universite class");
    }
    public void destroy(){
        System.out.println("destroy method for universite class");
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate_creation() {
        return date_creation;
    }

    public void setDate_creation(Date date_creation) {
        this.date_creation = date_creation;
    }

    public Set<Faculte> getFaculties() {
        return faculties;
    }

    public void setFaculties(Set<Faculte> faculties) {
        this.faculties = faculties;
    }

    public Personne getRecteur() {
        return recteur;
    }

    public void setRecteur(Personne recteur) {
        this.recteur = recteur;
    }

    @Override
    public String toString() {
        return "Universite{" +
                "nom='" + nom + '\'' +
                ", date_creation=" + date_creation +
                ", facultes=" + faculties +
                ", recteur=" + recteur +
                '}';
    }
}
