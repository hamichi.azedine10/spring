package service_XmlConfig;

import java.util.Date;

public class Doyen extends Personne{
    private Date dateocupationposte;

    public Date getDateocupationposte() {
        return dateocupationposte;
    }

    public void setDateocupationposte(Date dateocupationposte) {
        this.dateocupationposte = dateocupationposte;
    }

    @Override
    public String toString() {
        return "Doyen{" +
                "dateocupationposte=" + dateocupationposte +
                "} " + super.toString();
    }
}
