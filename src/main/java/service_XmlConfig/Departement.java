package service_XmlConfig;

import java.util.Set;

public class Departement {
    private  String nom_departement;
    private  ChefDeparement chefDeparement;
    private Set<Ensignant> ensignants;
    private int nombreEtudients ;

    public Faculte getFaculte() {
        return faculte;
    }

    public void setFaculte(Faculte faculte) {
        this.faculte = faculte;
    }

    private  Faculte faculte;

    public String getNom_departement() {
        return nom_departement;
    }

    public void setNom_departement(String nom_departement) {
        this.nom_departement = nom_departement;
    }

    public ChefDeparement getChefDeparement() {
        return chefDeparement;
    }

    public void setChefDeparement(ChefDeparement chefDeparement) {
        this.chefDeparement = chefDeparement;
    }

    public Set<Ensignant> getEnsignants() {
        return ensignants;
    }

    public void setEnsignants(Set<Ensignant> ensignants) {
        this.ensignants = ensignants;
    }

    public int getNombreEtudients() {
        return nombreEtudients;
    }

    public void setNombreEtudients(int nombreEtudients) {
        this.nombreEtudients = nombreEtudients;
    }

    @Override
    public String toString() {
        return "Departement{" +
                "nom_departement='" + nom_departement + '\'' +
                ", chefDeparement=" + chefDeparement +
                ", ensignants=" + ensignants +
                ", nombreEtudients=" + nombreEtudients +
                '}';
    }
}
