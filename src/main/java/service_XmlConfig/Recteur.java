package service_XmlConfig;

import java.util.Date;
public class Recteur  extends  Personne{

    private Date datenomination;

    public Date getDatenomination() {
        return datenomination;
    }

    public void setDatenomination(Date datenomination) {
        this.datenomination = datenomination;
    }

    @Override
    public String toString() {
        return "Recteur{" +
                "datenomination=" + datenomination +
                "} " + super.toString();
    }
}
